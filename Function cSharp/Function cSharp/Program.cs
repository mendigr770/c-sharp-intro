﻿using System;
using System.Data;
using System.Linq;

namespace function
{
    class Program
    {
        public static void printMatrix(int[,] matrix)
        {
            int rowLength = matrix.GetLength(0);
            int colLength = matrix.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                for (int j = 0; j < colLength; j++)
                {
                    Console.Write(string.Format("{0} ", matrix[i, j]));
                }
                Console.Write(Environment.NewLine + Environment.NewLine);
            }
            Console.WriteLine();
        }
        static public int getRundomNumber(int range)
        {
            Random random = new System.Random();
            return new Random().Next(range);
        }

        static public void calculator(int leftNumber, string operat, int rightNumber)
        {
            switch (operat)
            {
                case "+":
                case "-":
                case "*":
                    int res = (int)new DataTable().Compute($"{leftNumber} {operat} {rightNumber}", null);
                    Console.WriteLine($"{leftNumber} {operat} {rightNumber} -> {res}");
                    break;

                case "^":
                    double power = Math.Pow(leftNumber, rightNumber);
                    Console.WriteLine($"{leftNumber} {operat} {rightNumber} -> {power}");
                    break;
                case "/":
                    if (rightNumber == 0)
                    {
                        Console.WriteLine("cant devid by zero");
                        break;
                    }
                    double divided = (double)leftNumber / rightNumber;
                    Console.WriteLine($"{leftNumber} {operat} {rightNumber} -> {divided}");
                    break;
                default:
                    break;
            }
        }

        static void printArray(int[] arr)
        {
            for (int index = 0; index < arr.Length; ++index)
                Console.Write(arr[index] + " ");
            Console.Write("\n");
        }
        public static int[] sortBuble(int[] a, int option)
        {
            bool swapped = true;
            for (int i = 0; i < a.Length - 1 && swapped; i++)
            {
                swapped = false;
                for (int j = 1; j < a.Length - 1; j++)
                {
                    bool isNeedsToMakeSwap = option == 0 ? a[j - 1] < a[j] : a[j - 1] > a[j];
                    if (isNeedsToMakeSwap)
                    {
                        int temp = a[j - 1];
                        a[j - 1] = a[j];
                        a[j] = temp;
                        swapped = true;
                    }
                }
            }
            return a;
        }

        public static void rockScisscorsPaper(int firstPlayer, int secondPlayer)
        {
            if (firstPlayer % 3 + 1 == secondPlayer)
                Console.WriteLine("second player win");
            else if (secondPlayer % 3 + 1 == firstPlayer)
                Console.WriteLine("first player win");
            else
                Console.WriteLine("its a tie");
        }

        public static void shiftedString(char[] letters, string str)
        {
            char currChar;
            foreach (char letter in str)
            {

                currChar = letters.Contains(letter) ? (char)(letter + 10) : letter;
                if ((int)currChar > 122)
                {
                    currChar = (char)((int)(currChar % 122) + 96);
                }
                Console.Write(currChar);
            }
        }

        public static bool isPalindrome(string str)
        {
            char[] strAsCharArr = str.ToCharArray();
            for (int i = 0; i < strAsCharArr.Length; i++)
            {
                strAsCharArr[i] = Char.ToLower(strAsCharArr[i]);
            }
            Array.Reverse(strAsCharArr);
            return str.Equals(new string(strAsCharArr), StringComparison.OrdinalIgnoreCase);
        }

        public static int singelNumber(int[] numbers)
        {
            bool isSingel = false;
            int firstNumber = 0;
            for (; firstNumber < numbers.Length && !isSingel; firstNumber++)
            {
                isSingel = true;
                for (int secondNumber = firstNumber + 1; secondNumber < numbers.Length && isSingel; secondNumber++)
                {
                    if (numbers[firstNumber] == numbers[secondNumber])
                    {
                        isSingel = false;
                    }
                }
            }
            return isSingel ? numbers[firstNumber] : -1;
        }

        public static void gameOfLife(int[,] matrix)
        {
            Console.WriteLine("current level:");
            printMatrix(matrix);
            int row = matrix.GetLength(0);
            int col = matrix.GetLength(1);
            int[,] nextLevelMatrix = new int[row, col];
            Location[] locations = new Location[] { new Location(-1, -1), new Location(-1, 0),
                                                   new Location(-1, 1), new Location(0, 1),
                                                   new Location(1, 1), new Location(1, 0),
                                                   new Location(1, -1), new Location(0, -1)};
            int neighboursCounter;
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    neighboursCounter = 0;
                    foreach (Location location in locations)
                    {
                        try
                        {
                            if (matrix[i + location.Row, j + location.Col] == 1)
                            { neighboursCounter++; }
                        } catch (IndexOutOfRangeException e) { }
                    }
                    if (matrix[i, j] == 1)
                    {
                        if (neighboursCounter == 3 || neighboursCounter == 2)
                        { nextLevelMatrix[i, j] = 1; }
                        else 
                            { nextLevelMatrix[i, j] = 0; }
                    }
                    else if (neighboursCounter == 3)
                    {
                        nextLevelMatrix[i, j] = 1;
                    }
                }
            }
            Console.WriteLine("next level");
            printMatrix(nextLevelMatrix);
        }

        static void Main(string[] args)
        {
            //Console.WriteLine(getRundomNumber(23));

            //calculator(2, "/", 3);

            //int[] = new int[] {1, 2, 3, 4, 3, 2, 1};
            //int[] sortedArr = sortBuble(arr, 0);
            //printArray(sortedArr);

            //char[] letters = { 'i', 'o', 'z' };
            //string str = "hello wozld!";
            //shiftedString(letters, str);

            //const int ROCK = 1;
            //const int PAPER = 2;
            //const int SCISSCORS = 3;
            //rockScisscorsPaper(PAPER, PAPER);


            //Console.WriteLine(singelNumber(arr));

            int[,] arr = { { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 1 }, { 0, 0, 0 } };
            gameOfLife(arr);
            Console.ReadLine();
        }
    }
}
