﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace function
{
    class Location
    {
        public int row;
        public int col;

        public Location(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public int Row { get => this.row; set { } }
        public int Col { get => this.col; set { } }
    }
}
